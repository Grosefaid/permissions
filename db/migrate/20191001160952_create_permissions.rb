class CreatePermissions < ActiveRecord::Migration[5.2]
  def change
    create_table :permissions do |t|
      t.string :path
      t.string :command
      t.boolean :read
      t.boolean :write

      t.timestamps
    end
  end
end
