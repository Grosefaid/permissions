require 'rails_helper'

RSpec.describe Permission do

  context 'validation' do
    it 'allows create permissions with path or command only' do
      expect(build(:permission, path: 'C:/readme.txt', command: 'reboot')).to_not be_valid
      expect(build(:permission, path: nil, command: nil)).to_not be_valid
      expect(build(:permission, path: 'C:/readme.txt', command: nil, read: true)).to be_valid
      expect(build(:permission, path: nil, command: 'reboot')).to be_valid
    end

    it 'allows create resource permissions only with read or write params' do
      expect(build(:permission, path: 'C:/readme.txt', read: true)).to be_valid
      expect(build(:permission, path: 'C:/readme.txt', write: true)).to be_valid
      expect(build(:permission, path: 'C:/readme.txt', read: true, write: false)).to be_valid
      expect(build(:permission, path: 'C:/readme.txt', read: nil, write: nil)).to_not be_valid
    end

    it 'allows create only resource permissions with read or write params' do
      expect(build(:permission, path: 'C:/readme.txt', read: true, write: false)).to be_valid
      expect(build(:permission, command: 'reboot', read: true, write: false)).to_not be_valid
    end
  end

end