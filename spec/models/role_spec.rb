require 'rails_helper'

RSpec.describe Role do

  context 'validation' do
    it 'allows uniq names only' do
      create(:role, name: 'Manager')
      expect(build(:role, name: 'Manager')).to_not be_valid
    end
  end

end