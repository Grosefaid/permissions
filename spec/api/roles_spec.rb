require 'rails_helper'

RSpec.describe ProjectApi::Roles do

  context 'POST create' do
    it 'creates new role' do
      expect {post '/api/v1/roles/', params: {name: 'Admins'}}.to change(Role, :count).by(1)
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body).symbolize_keys.keys).to match_array [:id, :name, :created_at, :updated_at]
      expect(JSON.parse(response.body)['name']).to eq('Admins')
    end
  end

  context 'PUT update' do
    it 'updates role' do
      role = create(:role, name: 'Admins')
      put '/api/v1/roles/', params: {id: role.id, name: 'Managers'}
      expect(response.status).to eq(200)
      expect(role.reload.name).to eq('Managers')
    end

    it 'returns 404 status if role was not found' do
      role = create(:role, name: 'Admins')
      put '/api/v1/roles/', params: {id: 0, name: 'Managers'}
      expect(response.status).to eq(404)
      expect(JSON.parse(response.body)['message']).to eq('role not found')
      expect(role.reload.name).to eq('Admins')
    end
  end

  context 'DELETE delete' do
    it 'deletes role' do
      role = create(:role)
      expect {delete '/api/v1/roles/', params: {id: role.id}}.to change(Role, :count).by(-1)
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)['message']).to eq('role was destroyed')
    end

    it 'returns 404 status if role was not found' do
      delete '/api/v1/roles/', params: {id: 0}
      expect(response.status).to eq(404)
      expect(JSON.parse(response.body)['message']).to eq('role not found')
    end
  end

  context 'GET users_list' do
    it 'returns role users list' do
      role = create(:role, name: 'Admin')
      role.users << create(:user, name: 'Adam')
      role.users << create(:user, name: 'Eva')
      create(:role, name: 'Manager').users << create(:user, name: 'Dan')

      get '/api/v1/roles/users_list', params: {role_id: role.id}
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body).map{|res| res['name']}).to match_array(['Adam', 'Eva'])
    end

    it 'returns 404 status if user was not found' do
      get '/api/v1/roles/users_list', params: {role_id: 0}
      expect(response.status).to eq(404)
      expect(JSON.parse(response.body)['message']).to eq('role not found')
    end
  end

  context 'POST add_permissions' do
    it 'adds permissions to roles' do
      less_prm = create(:permission, command: 'less')
      reboot_prm = create(:permission, command: 'reboot')
      nano_prm = create(:permission, command: 'nano')
      admin_role = create(:role, name: 'Admin')
      manager_role = create(:role, name: 'Manager')

      post '/api/v1/roles/add_permissions', params: {role_ids: [admin_role.id, manager_role.id], permission_ids: [less_prm.id]}
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body)['message']).to eq('done')

      post '/api/v1/roles/add_permissions', params: {role_ids: [admin_role.id], permission_ids: [reboot_prm.id, nano_prm.id]}
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body)['message']).to eq('done')

      expect(admin_role.permissions.ids).to match_array([less_prm.id, reboot_prm.id, nano_prm.id])
      expect(manager_role.permissions.ids).to match_array([less_prm.id])
    end
  end

  context 'POST remove_permissions' do
    it 'removes permissions from roles' do
      less_prm = create(:permission, command: 'less')
      reboot_prm = create(:permission, command: 'reboot')
      admin_role = create(:role, name: 'Admin')
      manager_role = create(:role, name: 'Manager')

      admin_role.permissions << [reboot_prm, less_prm]
      manager_role.permissions << less_prm

      post '/api/v1/roles/remove_permissions', params: {role_ids: [admin_role.id, manager_role.id], permission_ids: [less_prm.id]}
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body)['message']).to eq('done')

      expect(admin_role.permissions.ids).to eq([reboot_prm.id])
      expect(manager_role.permissions.ids).to eq([])
    end
  end

end