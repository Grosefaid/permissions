require 'rails_helper'

RSpec.describe ProjectApi::Permissions do

  context 'POST create' do
    it 'creates new command permission' do
      expect {post '/api/v1/permissions/', params: {command: 'reboot'}}.to change(Permission, :count).by(1)
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body).symbolize_keys.keys).to match_array [:id, :path, :command, :read, :write, :created_at, :updated_at]
      expect(JSON.parse(response.body).symbolize_keys).to include(command: 'reboot', read: nil, write: nil, path: nil)
    end

    it 'creates new resource permission' do
      expect {post '/api/v1/permissions/', params: {path: 'C:/readme.txt', read: true, write: false}}.to change(Permission, :count).by(1)
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body).symbolize_keys.keys).to match_array [:id, :path, :command, :read, :write, :created_at, :updated_at]
      expect(JSON.parse(response.body).symbolize_keys).to include(command: nil, read: true, write: false, path: 'C:/readme.txt')
    end
  end

  context 'PUT update' do
    it 'updates permission' do
      permission = create(:permission, command: 'reboot')
      put '/api/v1/permissions/', params: {id: permission.id, command: 'nano'}
      expect(response.status).to eq(200)
      expect(permission.reload.command).to eq('nano')
    end

    it 'returns 404 status if permission was not found' do
      permission = create(:permission, command: 'reboot')
      put '/api/v1/permissions/', params: {id: 0, command: 'nano'}
      expect(response.status).to eq(404)
      expect(JSON.parse(response.body)['message']).to eq('permission not found')
      expect(permission.reload.command).to eq('reboot')
    end
  end

  context 'DELETE delete' do
    it 'deletes permission' do
      permission = create(:permission, command: 'reboot')
      expect {delete '/api/v1/permissions/', params: {id: permission.id}}.to change(Permission, :count).by(-1)
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)['message']).to eq('permission was destroyed')
    end

    it 'returns 404 status if permission was not found' do
      delete '/api/v1/permissions/', params: {id: 0}
      expect(response.status).to eq(404)
      expect(JSON.parse(response.body)['message']).to eq('permission not found')
    end
  end

  context 'GET users_list' do
    it 'returns permission users list' do
      prm = create(:permission, command: 'reboot')
      role = create(:role, name: 'Admins')

      role.permissions << prm
      create(:user, name: 'Adam').permissions << prm
      create(:user, name: 'Eva').roles << role
      create(:user, name: 'Sam').roles << create(:role, name: 'Manager')

      get '/api/v1/permissions/users_list', params: {permission_id: prm.id}
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body).map{|res| res['name']}).to match_array(['Adam', 'Eva'])
    end

    it 'returns 404 status if permission was not found' do
      get '/api/v1/permissions/users_list', params: {permission_id: 0}
      expect(response.status).to eq(404)
      expect(JSON.parse(response.body)['message']).to eq('permission not found')
    end
  end

  context 'GET roles_list' do
    it 'returns permission roles list' do
      prm = create(:permission, command: 'reboot')

      create(:role, name: 'Manager')
      admin_role = create(:role, name: 'Admin')
      superadmin_role = create(:role, name: 'SuperAdmin')

      superadmin_role.permissions << prm
      admin_role.permissions << prm

      get '/api/v1/permissions/roles_list', params: {permission_id: prm.id}
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body).map{|res| res['name']}).to match_array(['Admin', 'SuperAdmin'])
    end

    it 'returns 404 status if permission was not found' do
      get '/api/v1/permissions/roles_list', params: {permission_id: 0}
      expect(response.status).to eq(404)
      expect(JSON.parse(response.body)['message']).to eq('permission not found')
    end
  end
end