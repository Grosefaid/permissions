require 'rails_helper'

RSpec.describe ProjectApi::Users do

  context 'POST create' do
    it 'creates new user' do
      expect {post '/api/v1/users/', params: {name: 'Adam'}}.to change(User, :count).by(1)
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body).symbolize_keys.keys).to match_array [:id, :name, :created_at, :updated_at]
      expect(JSON.parse(response.body)['name']).to eq('Adam')
    end
  end

  context 'PUT update' do
    it 'updates user' do
      user = create(:user, name: 'Adam')
      put '/api/v1/users/', params: {id: user.id, name: 'Eva'}
      expect(response.status).to eq(200)
      expect(user.reload.name).to eq('Eva')
    end

    it 'returns 404 status if user was not found' do
      user = create(:user, name: 'Adam')
      put '/api/v1/users/', params: {id: 0, name: 'Eva'}
      expect(response.status).to eq(404)
      expect(JSON.parse(response.body)['message']).to eq('user not found')
      expect(user.reload.name).to eq('Adam')
    end
  end

  context 'DELETE delete' do
    it 'deletes user' do
      user = create(:user)
      expect {delete '/api/v1/users/', params: {id: user.id}}.to change(User, :count).by(-1)
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)['message']).to eq('user was destroyed')
    end

    it 'returns 404 status if user was not found' do
      delete '/api/v1/users/', params: {id: 0}
      expect(response.status).to eq(404)
      expect(JSON.parse(response.body)['message']).to eq('user not found')
    end
  end

  context 'GET roles_list' do
    it 'returns user roles list' do
      user = create(:user)
      user.roles << create(:role, name: 'Admin')
      user.roles << create(:role, name: 'SuperAdmin')
      create(:role, name: 'Manager')

      get '/api/v1/users/roles_list', params: {user_id: user.id}
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body).map{|res| res['name']}).to match_array(['Admin', 'SuperAdmin'])
    end

    it 'returns 404 status if user was not found' do
      get '/api/v1/users/roles_list', params: {user_id: 0}
      expect(response.status).to eq(404)
      expect(JSON.parse(response.body)['message']).to eq('user not found')
    end
  end

  context 'GET permissions_list' do
    it 'returns user permissions list' do
      user = create(:user)
      role = create(:role, name: 'Admin')
      ls_prm = create(:permission, command: 'ls')

      user.roles << role
      user.permissions << ls_prm
      role.permissions << ls_prm
      role.permissions << create(:permission, command: 'nano')

      create(:role, name: 'Manager')
      create(:permission, command: 'reboot')

      get '/api/v1/users/permissions_list', params: {user_id: user.id}
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body).map{|res| res['command']}).to match_array(['ls', 'nano'])
    end

    it 'returns 404 status if user was not found' do
      get '/api/v1/users/permissions_list', params: {user_id: 0}
      expect(response.status).to eq(404)
      expect(JSON.parse(response.body)['message']).to eq('user not found')
    end
  end

  context 'POST add_roles' do
    it 'adds roles to users' do
      manager_role = create(:role, name: 'Manager')
      admin_role = create(:role, name: 'Admin')
      super_admin_role = create(:role, name: 'SuperAdmin')
      user1 = create(:user, name: 'Adam')
      user2 = create(:user, name: 'Eva')

      post '/api/v1/users/add_roles', params: {user_ids: [user1.id, user2.id], role_ids: [manager_role.id]}
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body)['message']).to eq('done')

      post '/api/v1/users/add_roles', params: {user_ids: [user1.id], role_ids: [admin_role.id, super_admin_role.id]}
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body)['message']).to eq('done')

      expect(user1.roles.ids).to match_array([manager_role.id, admin_role.id, super_admin_role.id])
      expect(user2.roles.ids).to match_array([manager_role.id])
    end
  end

  context 'POST remove_roles' do
    it 'removes roles from user' do
      manager_role = create(:role, name: 'Manager')
      admin_role = create(:role, name: 'Admin')
      user1 = create(:user, name: 'Adam')
      user2 = create(:user, name: 'Eva')

      user1.roles << [manager_role, admin_role]
      user2.roles << manager_role

      post '/api/v1/users/remove_roles', params: {user_ids: [user1.id, user2.id], role_ids: [manager_role.id]}
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body)['message']).to eq('done')

      expect(user1.roles.ids).to eq([admin_role.id])
      expect(user2.roles.ids).to eq([])
    end
  end

  context 'POST add_permissions' do
    it 'adds permissions to users' do
      less_prm = create(:permission, command: 'less')
      reboot_prm = create(:permission, command: 'reboot')
      nano_prm = create(:permission, command: 'nano')
      user1 = create(:user, name: 'Adam')
      user2 = create(:user, name: 'Eva')

      post '/api/v1/users/add_permissions', params: {user_ids: [user1.id, user2.id], permission_ids: [less_prm.id]}
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body)['message']).to eq('done')

      post '/api/v1/users/add_permissions', params: {user_ids: [user1.id], permission_ids: [reboot_prm.id, nano_prm.id]}
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body)['message']).to eq('done')

      expect(user1.permissions.ids).to match_array([less_prm.id, reboot_prm.id, nano_prm.id])
      expect(user2.permissions.ids).to match_array([less_prm.id])
    end
  end

  context 'POST remove_permissions' do
    it 'removes permissions from user' do
      less_prm = create(:permission, command: 'less')
      reboot_prm = create(:permission, command: 'reboot')
      user1 = create(:user, name: 'Adam')
      user2 = create(:user, name: 'Eva')

      user1.permissions << [reboot_prm, less_prm]
      user2.permissions << less_prm

      post '/api/v1/users/remove_permissions', params: {user_ids: [user1.id, user2.id], permission_ids: [less_prm.id]}
      expect(response.status).to eq(201)
      expect(JSON.parse(response.body)['message']).to eq('done')

      expect(user1.permissions.ids).to eq([reboot_prm.id])
      expect(user2.permissions.ids).to eq([])
    end
  end
end