module AuthHelper

  def authenticate!
    error!('Unauthorized', 401) unless authorized_user
  end

  def authorized_user
    # auth stub
  end
end