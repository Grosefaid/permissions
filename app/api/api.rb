class API < Grape::API
  prefix 'api'
  version 'v1', using: :path
  format :json

  helpers ::AuthHelper

  mount ProjectApi::Users
  mount ProjectApi::Roles
  mount ProjectApi::Permissions
end