module ProjectApi
  class Users < Grape::API

    resource :users do
      desc 'List'
      get '/' do
        #TODO если планируется большое количество, то можно добавить pagination
        User.all
      end

      desc 'Create'
      params do
        requires :name, type: String
      end
      post '/' do
        user = User.create(name: params[:name])
        if user.new_record?
          status 500
          user.errors.messages
        else
          user.attributes
        end
      end

      desc 'Update'
      params do
        requires :id, type: Integer
        requires :name, type: String
      end
      put '/' do
        user = User.find_by_id(params[:id])
        if user
          user.update(name: params[:name])
          user.attributes
        else
          status 404
          {message: 'user not found'}
        end
      end

      desc 'Delete'
      params do
        requires :id, type: Integer
      end
      delete '/' do
        user = User.find_by_id(params[:id])
        if user
          user.destroy
          {message: 'user was destroyed'}
        else
          status 404
          {message: 'user not found'}
        end
      end

      desc 'Roles list'
      params do
        requires :user_id, type: Integer
      end
      get '/roles_list' do
        user = User.find_by_id(params[:user_id])
        if user
          user.roles
        else
          status 404
          {message: 'user not found'}
        end
      end

      desc 'Permissions list'
      params do
        requires :user_id, type: Integer
      end
      get '/permissions_list' do
        user = User.find_by_id(params[:user_id])
        if user
          (user.roles.map(&:permissions).flatten + user.permissions).uniq
        else
          status 404
          {message: 'user not found'}
        end
      end

      desc 'Add roles'
      params do
        requires :user_ids, type: Array
        requires :role_ids, type: Array
      end
      post '/add_roles' do
        users_with_role = User.includes(:roles).where(id: params[:user_ids], roles: {id: params[:role_ids]}).ids
        new_users_for_role = User.where(id: params[:user_ids] - users_with_role).ids
        new_values = []

        params[:role_ids].each do |role_id|
          new_users_for_role.each do |user_id|
            new_values << {role_id: role_id, user_id: user_id}
          end
        end

        RoleUser.import(new_values)
        {message: 'done'}
      end

      desc 'Remove roles'
      params do
        requires :user_ids, type: Array
        requires :role_ids, type: Array
      end
      post '/remove_roles' do
        RoleUser.where(user_id: params[:user_ids], role_id: params[:role_ids]).delete_all
        {message: 'done'}
      end

      desc 'Add permissions'
      params do
        requires :user_ids, type: Array
        requires :permission_ids, type: Array
      end
      post '/add_permissions' do
        users_with_permission = User.includes(:permissions).where(id: params[:user_ids], permissions: {id: params[:permission_ids]}).ids
        new_users_for_permission = User.where(id: params[:user_ids] - users_with_permission).ids
        new_values = []

        params[:permission_ids].each do |role_id|
          new_users_for_permission.each do |user_id|
            new_values << {permission_id: role_id, user_id: user_id}
          end
        end

        PermissionUser.import(new_values)
        {message: 'done'}
      end

      desc 'Remove permissions'
      params do
        requires :user_ids, type: Array
        requires :permission_ids, type: Array
      end
      post '/remove_permissions' do
        PermissionUser.where(user_id: params[:user_ids], permission_id: params[:permission_ids]).delete_all
        {message: 'done'}
      end
    end

  end
end