module ProjectApi
  class Permissions < Grape::API

    resource :permissions do
      desc 'List'
      get '/' do
        Permission.all
      end

      desc 'Commands list'
      get '/commands' do
        Permission.where.not(commands: nil)
      end

      desc 'Resources list'
      get '/resources' do
        Permission.where.not(path: nil)
      end

      desc 'Create'
      params do
        optional :path, type: String
        optional :command, type: String
        optional :read, type: Boolean
        optional :write, type: Boolean
      end
      post '/' do
        permission = Permission.create(path: params[:path], command: params[:command], read: params[:read], write: params[:write])
        if permission.new_record?
          status 500
          permission.errors.messages
        else
          permission.attributes
        end
      end

      desc 'Update'
      params do
        requires :id, type: Integer
        optional :path, type: String
        optional :command, type: String
        optional :read, type: Boolean
        optional :write, type: Boolean
      end
      put '/' do
        permission = Permission.find_by_id(params[:id])
        if permission
          permission.update(path: params[:path], command: params[:command], read: params[:read], write: params[:write])
        else
          status 404
          {message: 'permission not found'}
        end
      end

      desc 'Delete'
      params do
        requires :id, type: Integer
      end
      delete '/' do
        permission = Permission.find_by_id(params[:id])
        if permission
          permission.destroy
          {message: 'permission was destroyed'}
        else
          status 404
          {message: 'permission not found'}
        end
      end

      desc 'Users list'
      params do
        requires :permission_id, type: Integer
      end
      get '/users_list' do
        prm = Permission.find_by_id(params[:permission_id])
        if prm
          (prm.roles.map(&:users).flatten + prm.users).uniq
        else
          status 404
          {message: 'permission not found'}
        end
      end

      desc 'Roles list'
      params do
        requires :permission_id, type: Integer
      end
      get '/roles_list' do
        prm = Permission.find_by_id(params[:permission_id])
        if prm
          prm.roles
        else
          status 404
          {message: 'permission not found'}
        end
      end
    end

  end
end