module ProjectApi
  class Roles < Grape::API

    resource :roles do
      desc 'List'
      get '/' do
        Role.all
      end

      desc 'Create'
      params do
        requires :name, type: String
      end
      post '/' do
        role = Role.create(name: params[:name])
        if role.new_record?
          status 500
          role.errors.messages
        else
          role.attributes
        end
      end

      desc 'Update'
      params do
        requires :id, type: Integer
        requires :name, type: String
      end
      put '/' do
        role = Role.find_by_id(params[:id])
        if role
          role.update(name: params[:name])
          role.attributes
        else
          status 404
          {message: 'role not found'}
        end
      end

      desc 'Delete'
      params do
        requires :id, type: Integer
      end
      delete '/' do
        role = Role.find_by_id(params[:id])
        if role
          role.destroy
          {message: 'role was destroyed'}
        else
          status 404
          {message: 'role not found'}
        end
      end

      desc 'Users list'
      params do
        requires :role_id, type: Integer
      end
      get '/users_list' do
        role = Role.find_by_id(params[:role_id])
        if role
          role.users
        else
          status 404
          {message: 'role not found'}
        end
      end

      desc 'Add permissions'
      params do
        requires :role_ids, type: Array
        requires :permission_ids, type: Array
      end
      post '/add_permissions' do
        roles_with_permission = Role.includes(:permissions).where(id: params[:role_ids], permissions: {id: params[:permission_ids]}).ids
        new_roles_for_permission = Role.where(id: params[:role_ids] - roles_with_permission).ids
        new_values = []

        params[:permission_ids].each do |prm_id|
          new_roles_for_permission.each do |role_id|
            new_values << {permission_id: prm_id, role_id: role_id}
          end
        end

        PermissionRole.import(new_values)
        {message: 'done'}
      end

      desc 'Remove permissions'
      params do
        requires :role_ids, type: Array
        requires :permission_ids, type: Array
      end
      post '/remove_permissions' do
        PermissionRole.where(role_id: params[:role_ids], permission_id: params[:permission_ids]).delete_all
        {message: 'done'}
      end
    end

  end
end