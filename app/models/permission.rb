class Permission < ApplicationRecord
  has_and_belongs_to_many :users
  has_and_belongs_to_many :roles

  # проверяем наличия пути или команды
  validates_presence_of :path,if: ->(permission) {permission.command.blank?}
  validates_absence_of :command, if: ->(permission) {permission.path.present?}

  # проверяем что хотя бы одно из прав установлено
  validates_presence_of :read, if: ->(permission) {permission.path.present? && permission.write.blank?}
  validates_presence_of :write, if: ->(permission) {permission.path.present? && permission.read.blank?}

  # проверяем что права устанавливаются только для файлов/папок
  validates_absence_of :read, :write, if: ->(permission) {permission.command.present?}
end
